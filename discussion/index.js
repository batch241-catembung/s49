//mock database
//let posts = [];

//post ID
//let count = 1; 

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((data)=>showPosts(data))
//reactive DOM with fetch (CRUD operation)

//Add post data
// 'e' means event
document.querySelector("#form-add-post").addEventListener('submit', (e)=>{
    //prevent the default behavior of an event
    //to prevent the page from reloading (default behavior of submit)
    e.preventDefault();
    // posts.push({
    //     id: count, 
    //     title: document.querySelector('#txt-title').value,
    //     body: document.querySelector('#txt-body').value
    // });
    // count++
    // console.log(posts);
    // alert("post successfully added!");
    // showPosts();
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
    })
    .then((response)=> response.json())
    .then((data)=>{
        console.log(data);
        alert("post successfully added!");
        document.querySelector('#text-title').value = null;
        document.querySelector('#text-body').value = null;
    });
});

//retrieve post 

const showPosts = (posts) =>{
    //variable that will contain all the posts 
    let postEntries = "";
    //looping through array items
    posts.forEach((post)=>{
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>


        `
    });
    console.log(postEntries);
    document.querySelector('#div-post-entries').innerHTML = postEntries
}


//edit post (edit button)
const editPost = (id)=>{
    //the function first uses the querySelector() method to get the element with the id "post-title${id}" and "#post-body-${id}" and asssigns its innerHTML property to the title variable with the same body
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

     document.querySelector('#btn-submit-update').removeAttribute('disabled')
};

//update post (update button)
document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
    e.preventDefault();

    // for(let i = 0; i<posts.length; i++){
    //     if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
    //         posts[i].title = document.querySelector("#txt-edit-title").value;
    //         posts[i].body = document.querySelector("#txt-edit-body").value;

    //         showPosts(posts);
    //         alert("Post successfully updated");
    //     }

    // }

    fetch('https://jsonplaceholder.typicode.com/posts/1',{
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
    })
    .then((response)=>response.json())
    .then((data)=>{
        console.log(data);
        alert("Post successfully updated");
        //reset the edit post form input fields
        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        //setAttribute() - add or sets an attribute on HTML element
        document.querySelector('#btn-submit-update').setAttribute('disabled', true)
    })
});

activity delete
const deletePost = (id) => {
 document.querySelector(`#post-${id}`).remove();     
}

//ALTERNATIVE ANSWER
// const deletePost = (id) => {
//       fetch('https://jsonplaceholder.typicode.com/posts/1',{
//         method: 'DELETE'
// })
//     .then((response)=>response.json())
//     .then((data)=>{
//         console.log(data); 
//     }) 
// }













// const deletePost = (id) => {

//     for(let i = 0; i < posts.length; i++){
//         if(posts[i].id.toString() === id){
//             posts.splice(i, 1); 
//             break;
//         }
//     }
//     alert("Post successfully deleted!");
//     showPosts();
// }





